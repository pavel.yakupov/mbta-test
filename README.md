# Mbta Test

The application allows you to choose a train stop and see the last three notes 
on the schedule with statuses. If less than 10 minutes left, the status 
of "Arriving soon" remains, otherwise "Scheduled". The displayed schedule 
will be updated without reloading the page, if the arrival time was equal to the current time, 
the schedule will be recalculated. If up to one of the arrivals remains less than 10 minutes 
the status will be changed to "Arriving soon".

This is a brief summary about an App. You can find details about structure, modules and testing above. 

### Structure
There is an App with standard architecture. All components have onPush change detection strategy. The App includes 4 modules: 
- **Core module** uses for singleton things shared throughout the application; 
- **Shared module** uses for shared services, components and etc.;
- **Ant module** is a UI kit module, it imports AntUI modules and exports them;
- **Mbta module** keeps main logic.

### Mbta Module
The Mbta module includes one component-controller (`SchedulePageComponent`) and two pure components (`ScheduleComponent` and `SearchComponent`).
`SchedulePageComponent` projects data between state and pure components using
custom provider with information about stops, schedules and loading statuses. 
In addition, this component has another dependency. This is a facade. The facade 
hides logic with additional dependence. The component-controller depends only 
on the observed data that will occur through the token and emits the events 
that are processed at the facade level.

`SchedulePageComponent`'s provider has an interesting thing. This is `schedules$` property.
It's a RxJS stream which checks every 5 seconds information about concrete schedule and manage
statuses using the client side.

```
export const providers: Provider[] = [
  {
    provide: SchedulePageTokens.DATA,
    useFactory: (state: StateService) => ({
      //...
      schedules$: timer(0, 5000).pipe(
        switchMap(() => state.schedules$.asObservable().pipe(
          filter(Boolean),
          map((schedules: Schedule[]) => ScheduleUtils.getNearestSchedules(schedules).slice(0, 3))
        ))
      ),
      //...
    }),
    deps: [StateService]
  },
];
```

Also, the App has another function for reloading data using the server side. It provides
schedules for a particular stop and then reload the data for that stop every `this.reloadTime`
ms. If the stop is changed it will complete the previous timer using the `takeWhile`'s predicate 
and start downloading actual data for a new stop.

```
this.schedulesService.find({ 'filter[stop]': stopId }).pipe(
  first(),
  tap((schedules: Schedule[]) => this.stateService.schedules$.next(schedules)),
  tap(() => this.currentStopId$.next(stopId)),
  tap(() => this.stateService.schedulesLoading$.next(false)),
  switchMap(() => timer(this.reloadTime, this.reloadTime).pipe(
    takeWhile(() => this.currentStopId$.getValue() === stopId),
    switchMap(() => this.schedulesService.find({ 'filter[stop]': stopId }).pipe(
      first(),
      tap((schedules: Schedule[]) => this.stateService.schedules$.next(schedules))
    ))
  )),
  catchError(err => {
    this.stateService.schedulesLoading$.next(false);

    return throwError(err);
  })
).subscribe();
```


### Tests
All components of an App are get ready to testing. As a test example `SearchComponent` can be checked.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
