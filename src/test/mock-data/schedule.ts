import { Schedule } from '../../app/modules/mbta/interfaces/schedule';


export namespace TestData {
  export const mockSchedule1: Schedule = {
    "attributes": {
      "arrival_time": "2022-06-03T06:47:00",
      "departure_time": "2022-06-03T06:47:00-04:00",
      "direction_id": 1,
      "drop_off_type": 0,
      "pickup_type": 0,
      "stop_headsign": null,
      "stop_sequence": 12,
      "timepoint": false
    },
    "id": "schedule-51106751-772-12",
    "relationships": {
      "prediction": {
        "data": null
      },
      "route": {
        "data": {
          "id": "35",
          "type": "route"
        }
      },
      "stop": {
        "data": {
          "id": "772",
          "type": "stop"
        }
      },
      "trip": {
        "data": {
          "id": "51106751",
          "type": "trip"
        }
      }
    },
    "type": "schedule"
  };

  export const mockSchedule2: Schedule = {
    "attributes": {
      "arrival_time": "2022-06-03T08:55:00",
      "departure_time": "2022-06-03T08:55:00-04:00",
      "direction_id": 1,
      "drop_off_type": 0,
      "pickup_type": 0,
      "stop_headsign": null,
      "stop_sequence": 12,
      "timepoint": false
    },
    "id": "schedule-51106751-772-12",
    "relationships": {
      "prediction": {
        "data": null
      },
      "route": {
        "data": {
          "id": "35",
          "type": "route"
        }
      },
      "stop": {
        "data": {
          "id": "772",
          "type": "stop"
        }
      },
      "trip": {
        "data": {
          "id": "51106751",
          "type": "trip"
        }
      }
    },
    "type": "schedule"
  };
}
