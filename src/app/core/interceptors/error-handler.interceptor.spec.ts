import { TestBed } from '@angular/core/testing';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { ErrorHandlerInterceptor } from './error-handler.interceptor';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';


describe('ErrorHandlerInterceptor', () => {
  let httpClient: HttpClient;
  let messageService: NzMessageService;
  let httpTestingController: HttpTestingController;

  beforeEach(() =>{
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ErrorHandlerInterceptor,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorHandlerInterceptor,
          multi: true
        },
        {
          provide: NzMessageService,
          useValue: {
            create(type: string, message: string) {}
          }
        }
      ]
    });

    httpClient = TestBed.inject(HttpClient);
    messageService = TestBed.inject(NzMessageService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => httpTestingController.verify());

  it('should be created', () => {
    const interceptor: ErrorHandlerInterceptor = TestBed.inject(ErrorHandlerInterceptor);
    expect(interceptor).toBeTruthy();
  });


  it('should not create an error message when request is ok', (done) => {
    spyOn(messageService, 'create');

    httpClient.get('/').subscribe(() => done());

    const req: TestRequest = httpTestingController.expectOne('/');
    req.flush(null);

    expect(messageService.create).not.toHaveBeenCalled();
  });

  xit('should create an error message when request fails', (done) => {

  });
});
