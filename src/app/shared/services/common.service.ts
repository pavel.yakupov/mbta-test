import { map, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiResponse } from '../../modules/mbta/interfaces/response';
import { environment } from '../../../environments/environment';


@Injectable()
export abstract class CommonService<T> {
  protected abstract entityPath: string;

  constructor(
    protected readonly httpClient: HttpClient
  ) {}

  find(params: { [key: string]: string }): Observable<T[]> {
    return this.httpClient.get<ApiResponse<T>>(`${environment.apiUrl}/${this.entityPath}`, { params }).pipe(
      map(({ data }: ApiResponse<T>) => data)
    );
  }
}
