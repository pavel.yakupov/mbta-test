import { of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { CommonService } from './common.service';
import { HttpClient } from '@angular/common/http';


describe('CommonService', () => {
  let service: CommonService<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CommonService,
        {
          provide: HttpClient,
          useValue: {
            get(url: string, options: {params: { [key: string]: string }}) {
              return of({ data: [ {id: 1} ] })
            }
          }
        }
      ]
    });
    service = TestBed.inject(CommonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should project data when try to find entities', (done: DoneFn) => {
    service.find({}).subscribe(
      (value => {
        expect(value).toEqual([ {id: 1} ]);
        done();
      })
    );
  });
});
