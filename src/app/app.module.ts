import { NgModule } from '@angular/core';
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { MbtaModule } from './modules/mbta/mbta.module';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './core/components/app/app.component';


@NgModule({
  imports: [
    MbtaModule,
    CoreModule,
    BrowserModule,
    AppRoutingModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
