import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Stop } from '../../interfaces/stop';
import { Schedule } from '../../interfaces/schedule';


@Injectable()
export class StateService {
  stops$: BehaviorSubject<Stop[]> = new BehaviorSubject<Stop[]>([]);
  currentStopsId$: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  stopsLoading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  schedules$: BehaviorSubject<Schedule[]> = new BehaviorSubject<Schedule[]>(null);
  schedulesLoading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
}
