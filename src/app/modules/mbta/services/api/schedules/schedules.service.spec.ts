import { TestBed } from '@angular/core/testing';

import { SchedulesService } from './schedules.service';
import { CommonService } from '../../../../../shared/services/common.service';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

describe('SchedulesService', () => {
  let service: SchedulesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SchedulesService,
        {
          provide: CommonService,
          useValue: {
            find(params: { [key: string]: string }) {
              return of([])
            }
          }
        },
        {
          provide: HttpClient,
          useValue: {
            get(url: string, options: {params: { [key: string]: string }}) {
              return of([])
            }
          }
        }
      ]
    });
    service = TestBed.inject(SchedulesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
