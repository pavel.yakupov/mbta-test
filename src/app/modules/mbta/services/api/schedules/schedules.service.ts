import { Injectable } from '@angular/core';
import { CommonService } from '../../../../../shared/services/common.service';
import { Schedule } from '../../../interfaces/schedule';


@Injectable()
export class SchedulesService extends CommonService<Schedule> {
  protected override entityPath: string = 'schedules';
}
