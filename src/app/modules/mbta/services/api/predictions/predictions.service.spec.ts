import { TestBed } from '@angular/core/testing';

import { PredictionsService } from './predictions.service';
import { CommonService } from '../../../../../shared/services/common.service';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

describe('PredictionsService', () => {
  let service: PredictionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PredictionsService,
        {
          provide: CommonService,
          useValue: {
            find(params: { [key: string]: string }) {
              return of([])
            }
          }
        },
        {
          provide: HttpClient,
          useValue: {
            get(url: string, options: {params: { [key: string]: string }}) {
              return of([])
            }
          }
        }
      ]
    });
    service = TestBed.inject(PredictionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
