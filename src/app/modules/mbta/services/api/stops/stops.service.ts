import { Injectable } from '@angular/core';
import { Stop } from '../../../interfaces/stop';
import { CommonService } from '../../../../../shared/services/common.service';


@Injectable()
export class StopsService extends CommonService<Stop> {
  protected override entityPath: string = 'stops';
}
