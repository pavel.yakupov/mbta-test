import { of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { StopsService } from './stops.service';
import { HttpClient } from '@angular/common/http';


describe('StopsService', () => {
  let service: StopsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        StopsService,
        {
          provide: HttpClient,
          useValue: {
            get(url: string, options: {params: { [key: string]: string }}) {
              return of([])
            }
          }
        }
      ]
    });
    service = TestBed.inject(StopsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
