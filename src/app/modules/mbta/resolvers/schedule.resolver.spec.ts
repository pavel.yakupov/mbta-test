import { TestBed } from '@angular/core/testing';

import { ScheduleResolver } from './schedule.resolver';
import { CommonService } from '../../../shared/services/common.service';
import { BehaviorSubject, of } from 'rxjs';
import { StopsService } from '../services/api/stops/stops.service';
import { StateService } from '../services/state/state.service';

describe('ScheduleResolver', () => {
  let resolver: ScheduleResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ScheduleResolver,
        {
          provide: StopsService,
          useValue: {
            find(params: { [key: string]: string }) {
              return of([])
            }
          }
        },
        {
          provide: StateService,
          useValue: {
            stops$: new BehaviorSubject([]),
            stopsLoading$: new BehaviorSubject(false),
          }
        },
      ]
    });
    resolver = TestBed.inject(ScheduleResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
