import { Stop } from '../interfaces/stop';
import { Injectable } from '@angular/core';
import { finalize, first, Observable, of, tap } from 'rxjs';
import { StateService } from '../services/state/state.service';
import { StopsService } from '../services/api/stops/stops.service';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';


@Injectable()
export class ScheduleResolver implements Resolve<boolean> {
  constructor(
    private readonly stopsService: StopsService,
    private readonly stateService: StateService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.stateService.stopsLoading$.next(true);

    this.stopsService.find({
      'filter[route_type]': '0,1,2,5,12'
    }).pipe(
      first(),
      tap((stops: Stop[]) => this.stateService.stops$.next(stops)),
      finalize(() => this.stateService.stopsLoading$.next(false))
    ).subscribe();

    return of(true);
  }
}
