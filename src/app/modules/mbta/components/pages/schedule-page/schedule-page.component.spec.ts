import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SchedulePageComponent } from './schedule-page.component';
import { SchedulePageFacade } from './schedule-page.facade';
import { SchedulePageTokens } from './schedule-page.providers';
import { of } from 'rxjs';


describe('SchedulePageComponent', () => {
  let component: SchedulePageComponent;
  let fixture: ComponentFixture<SchedulePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedulePageComponent ],
      providers: []
    })
    .compileComponents();

    TestBed.overrideComponent(
      SchedulePageComponent,
      {
        set:
          {
            providers: [
              {
                provide: SchedulePageFacade,
                useValue: {
                  findSchedule(value: string) {}
                }
              },
              {
                provide: SchedulePageTokens.DATA,
                useValue: {
                  schedules$: of([]),
                  stopSourceItems$: of([]),
                  stopsLoading$: of(false),
                  schedulesLoading$: of(false),
                }
              },
          ]
        }
      }
    );

    fixture = TestBed.createComponent(SchedulePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
