import { SchedulePageFacade } from './schedule-page.facade';
import { providers, SchedulePageTokens } from './schedule-page.providers';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';


@Component({
  selector: 'app-schedule-page',
  templateUrl: './schedule-page.component.html',
  styleUrls: ['./schedule-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [providers, SchedulePageFacade]
})
export class SchedulePageComponent {
  constructor(
    private readonly facade: SchedulePageFacade,
    @Inject(SchedulePageTokens.DATA) public readonly data: SchedulePageTokens.Data
  ) {}

  searchChanges(value: string): void {
    this.facade.findSchedule(value);
  }
}
