import { Injectable } from '@angular/core';
import { Schedule } from '../../../interfaces/schedule';
import { StateService } from '../../../services/state/state.service';
import { SchedulesService } from '../../../services/api/schedules/schedules.service';
import { BehaviorSubject, catchError, first, switchMap, takeWhile, tap, throwError, timer } from 'rxjs';


@Injectable()
export class SchedulePageFacade {
  private reloadTime: number = 1000 * 60 * 3;
  private currentStopId$: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(
    private readonly stateService: StateService,
    private readonly schedulesService: SchedulesService,
  ) {}

  findSchedule(stopId: string): void {
    this.currentStopId$.next(stopId);
    this.stateService.schedulesLoading$.next(true);

    this.schedulesService.find({ 'filter[stop]': stopId }).pipe(
      first(),
      tap((schedules: Schedule[]) => this.stateService.schedules$.next(schedules)),
      tap(() => this.currentStopId$.next(stopId)),
      tap(() => this.stateService.schedulesLoading$.next(false)),
      switchMap(() => timer(this.reloadTime, this.reloadTime).pipe(
        takeWhile(() => this.currentStopId$.getValue() === stopId),
        switchMap(() => this.schedulesService.find({ 'filter[stop]': stopId }).pipe(
          first(),
          tap((schedules: Schedule[]) => this.stateService.schedules$.next(schedules))
        ))
      )),
      catchError(err => {
        this.stateService.schedulesLoading$.next(false);

        return throwError(err);
      })
    ).subscribe();
  }
}
