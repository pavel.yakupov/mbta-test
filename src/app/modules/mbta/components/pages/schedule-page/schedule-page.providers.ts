import { ScheduleUtils } from '../../../utils';
import { Stop } from '../../../interfaces/stop';
import { Schedule } from '../../../interfaces/schedule';
import { InjectionToken, Provider } from '@angular/core';
import { filter, map, Observable, switchMap, timer } from 'rxjs';
import { StateService } from '../../../services/state/state.service';
import { AutocompleteDataSourceItem } from 'ng-zorro-antd/auto-complete';


export namespace SchedulePageTokens {
  export const DATA: InjectionToken<Observable<boolean>> = new InjectionToken<Observable<boolean>>(
    'A stream with schedule page data'
  );

  export interface Data {
    stopsLoading$: Observable<boolean>;
    schedules$: Observable<Schedule[]>;
    schedulesLoading$: Observable<boolean>;
    stopSourceItems$: Observable<AutocompleteDataSourceItem[]>;
  }
}

export const providers: Provider[] = [
  {
    provide: SchedulePageTokens.DATA,
    useFactory: (state: StateService) => ({
      stopSourceItems$: state.stops$.asObservable().pipe(
        map((stops: Stop[]) => stops.map(
            (stop: Stop) => ({ value: stop.id, label: stop.attributes.description })
          )
        )
      ),
      schedules$: timer(0, 5000).pipe(
        switchMap(() => state.schedules$.asObservable().pipe(
          filter(Boolean),
          map((schedules: Schedule[]) => ScheduleUtils.getNearestSchedules(schedules).slice(0, 3))
        ))
      ),
      stopsLoading$: state.stopsLoading$.asObservable(),
      schedulesLoading$: state.schedulesLoading$.asObservable(),
    }),
    deps: [StateService]
  },
];
