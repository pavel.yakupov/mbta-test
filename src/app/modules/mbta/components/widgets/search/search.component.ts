import { FormControl } from '@angular/forms';
import { Subject, takeUntil, tap } from 'rxjs';
import { AutocompleteDataSourceItem } from 'ng-zorro-antd/auto-complete';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchComponent implements OnInit, OnDestroy {
  @Input() entities: AutocompleteDataSourceItem[] = [];

  @Output() searchChanges: EventEmitter<string> = new EventEmitter<string>();

  searchControl: FormControl = new FormControl(null);

  private onDestroy$: Subject<void> = new Subject<void>();

  ngOnInit(): void {
    this.handleFormValueChanges();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  private handleFormValueChanges(): void {
    this.searchControl.valueChanges.pipe(
      takeUntil(this.onDestroy$),
      tap((value: string) => this.searchChanges.emit(value))
    ).subscribe();
  }
}
