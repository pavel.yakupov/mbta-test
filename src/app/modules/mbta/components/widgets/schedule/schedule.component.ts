import { ScheduleUtils } from '../../../utils';
import { Schedule } from '../../../interfaces/schedule';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';


@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScheduleComponent {
  @Input() schedules: Schedule[];

  getStatus(date: string): string {
    return ScheduleUtils.getArrivalStatus(date);
  }
}
