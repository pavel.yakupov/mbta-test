import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { ScheduleComponent } from './schedule.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChangeDetectionUtils } from '../../../../../../test/utils';
import { TestData } from '../../../../../../test/mock-data/schedule';


describe('ScheduleComponent', () => {
  let component: ScheduleComponent;
  let fixture: ComponentFixture<ScheduleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain "There are no matching entries" if @Input schedules is empty', async () => {
    component.schedules = [];

    await ChangeDetectionUtils.runOnPushChangeDetection(fixture);

    const element: DebugElement = fixture.debugElement.query(By.css('p'));

    expect(element).toBeTruthy();
    expect(fixture.debugElement.query(By.css('p'))?.nativeElement?.textContent).toContain('There are no matching entries');
  });

  it('should not contain "There are no matching entries" if @Input schedules is not empty', async () => {
    component.schedules = [TestData.mockSchedule1];

    await ChangeDetectionUtils.runOnPushChangeDetection(fixture);

    const element: DebugElement = fixture.debugElement.query(By.css('p'));

    expect(element).toBeNull();
  });

  it('should contain list of schedules if @Input schedules is not empty', async () => {
    component.schedules = [TestData.mockSchedule1, TestData.mockSchedule2];

    await ChangeDetectionUtils.runOnPushChangeDetection(fixture);

    const elements: DebugElement[] = fixture.debugElement.queryAll(By.css('.schedule-item'));

    expect(elements.length).toEqual(2);
  });

  it('items should contain correct status depends on arrival time', async () => {
    jasmine.clock().install();
    jasmine.clock().mockDate(new Date('2022-06-03T06:41:00'));

    component.schedules = [TestData.mockSchedule1, TestData.mockSchedule2];

    await ChangeDetectionUtils.runOnPushChangeDetection(fixture);

    const elements: DebugElement[] = fixture.debugElement.queryAll(By.css('.schedule-item'));

    expect(elements[0].query(By.css('.schedule-item-status')).nativeElement.textContent).toEqual('Arriving soon');
    expect(elements[1].query(By.css('.schedule-item-status')).nativeElement.textContent).toEqual('Scheduled');

    jasmine.clock().uninstall();
  });

  it('items should contain correct date format', async () => {
    component.schedules = [TestData.mockSchedule1, TestData.mockSchedule2];

    await ChangeDetectionUtils.runOnPushChangeDetection(fixture);

    const elements: DebugElement[] = fixture.debugElement.queryAll(By.css('.schedule-item'));

    expect(elements[0].query(By.css('.schedule-item-date')).nativeElement.textContent).toEqual('06.03.22 6:47');
    expect(elements[1].query(By.css('.schedule-item-date')).nativeElement.textContent).toEqual('06.03.22 8:55');
  });
});
