export interface ApiResponse<T> {
  data: T[],
  jsonapi: {
    version: string;
  }
}
