export interface Stop {
  id: string;
  links: Links;
  type: string;
  attributes: Attributes;
  relationships: Relationships;
}

export interface Attributes {
  name: string;
  address?: any;
  at_street?: any;
  on_street?: any;
  latitude: number;
  longitude: number;
  description: string;
  municipality: string;
  vehicle_type: number;
  location_type: number;
  platform_code: string;
  platform_name: string;
  wheelchair_boarding: number;
}

export interface Links {
  self: string;
}

export interface FacilitiesLinks {
  related: string;
}

export interface Facilities {
  links: FacilitiesLinks;
}

export interface Data {
  id: string;
  type: string;
}

export interface ParentStation {
  data: Data;
}

export interface ZoneData {
  id: string;
  type: string;
}

export interface Zone {
  data: ZoneData;
}

export interface Relationships {
  zone: Zone;
  facilities: Facilities;
  parent_station: ParentStation;
}

