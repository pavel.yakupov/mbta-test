export interface Schedule {
  attributes: Attributes;
  id: string;
  relationships: Relationships;
  type: string;
}

export interface Attributes {
  arrival_time: string;
  departure_time: string;
  direction_id: number;
  drop_off_type: number;
  pickup_type: number;
  stop_headsign?: any;
  stop_sequence: number;
  timepoint: boolean;
}

export interface Prediction {
  data?: any;
}

export interface Data {
  id: string;
  type: string;
}

export interface Route {
  data: Data;
}

export interface StopData {
  id: string;
  type: string;
}

export interface Stop {
  data: StopData;
}

export interface TripData {
  id: string;
  type: string;
}

export interface Trip {
  data: TripData;
}

export interface Relationships {
  prediction: Prediction;
  route: Route;
  stop: Stop;
  trip: Trip;
}


