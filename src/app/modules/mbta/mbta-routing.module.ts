import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchedulePageComponent } from './components/pages/schedule-page/schedule-page.component';
import { ScheduleResolver } from './resolvers/schedule.resolver';


const routes: Routes = [
  {
    path: '',
    component: SchedulePageComponent,
    resolve: {
      schedule: ScheduleResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MbtaRoutingModule {}
