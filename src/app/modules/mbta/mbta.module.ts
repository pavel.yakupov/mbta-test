import { NgModule } from '@angular/core';
import { AntModule } from '../ant/ant.module';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MbtaRoutingModule } from './mbta-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { StateService } from './services/state/state.service';
import { ScheduleResolver } from './resolvers/schedule.resolver';
import { StopsService } from './services/api/stops/stops.service';
import { SchedulesService } from './services/api/schedules/schedules.service';
import { SearchComponent } from './components/widgets/search/search.component';
import { PredictionsService } from './services/api/predictions/predictions.service';
import { ScheduleComponent } from './components/widgets/schedule/schedule.component';
import { SchedulePageComponent } from './components/pages/schedule-page/schedule-page.component';


@NgModule({
  declarations: [
    SearchComponent,
    ScheduleComponent,
    SchedulePageComponent,
  ],
  imports: [
    AntModule,
    CommonModule,
    SharedModule,
    HttpClientModule,
    MbtaRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [
    StopsService,
    StateService,
    SchedulesService,
    ScheduleResolver,
    PredictionsService,
  ]
})
export class MbtaModule { }
