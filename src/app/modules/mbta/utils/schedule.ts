import { Schedule } from '../interfaces/schedule';

export const getArrivalStatus = (date: string): string => {
  const arrival: Date = new Date(date);
  const current: Date = new Date();

  const diff: number = arrival.getTime() - current.getTime();

  if (diff <= 0) {
    return 'Arrived';
  }

  if (diff / 60000 <= 10) {
    return 'Arriving soon';
  }

  return 'Scheduled';
};

export const getNearestSchedules = (schedules: Schedule[]): Schedule[] => schedules
  .filter(
    (schedule: Schedule) => new Date(schedule.attributes.arrival_time).getTime() > Date.now()
  )
  .sort(
    (a: Schedule, b: Schedule) => new Date(a.attributes.arrival_time).getTime() - new Date(b.attributes.arrival_time).getTime()
  );
