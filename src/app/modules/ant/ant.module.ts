import { NgModule } from '@angular/core';
import en from '@angular/common/locales/en';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzInputModule } from 'ng-zorro-antd/input';
import { en_US, NZ_I18N } from 'ng-zorro-antd/i18n';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { CommonModule, registerLocaleData } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


registerLocaleData(en);

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NzSpinModule,
    NzInputModule,
    NzSelectModule,
    BrowserAnimationsModule,
  ],
  exports: [
    NzSpinModule,
    NzInputModule,
    NzSelectModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: NZ_I18N,
      useValue: en_US
    }
  ]
})
export class AntModule {}
